import { createStore } from "vuex"

export type State = {name: string}

const state: State = {name: "Doo"}

const store = createStore({
   state
})

export default store