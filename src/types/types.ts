export type yPosition = 'top' | 'center' | 'bottom'
export type xPosition = 'left' | 'center' | 'right'

export type sceneInterface = {
  settings: sceneSettingsInterface,
  controls: sceneControlsInterface,
  layers: layerInterface[] | []
}

export type sceneSettingsInterface = {
  duration: number
  ratio?: number | 'cover'
}

export type sceneControlsInterface = {
  buttons?: [{
    name: string
    icon: string
    default?: boolean
  }]
}

export type layerInterface = {
  name: string
  toggle?: string
  transition?: 'fade'
  blocks: blockConfigInterface[]
}

export type blockConfigInterface = {
  type: string
  timeInterval: [number, number]
  size?: [number | 'auto', number | 'auto'] | 'full',
  position?: [yPosition, xPosition] | [number, number],
  transition?: 'fade' | 'slide-left' | 'slide-right'
  config?: textConfigInterface | titleConfigInterface | imageConfigInterface
}

export type blockStyleInterface = {
  left?: string
  top?: string
  width?: string
  height?: string
}

export type textConfigInterface = {
  text: string
  style?: {}
}

export type imageConfigInterface = {
  src: string
  alt?: string
  style?: {}
}

export type titleConfigInterface = {
  title: string
  subTitle?: string
  style?: {}
  titleStyle?: {}
  subTitleStyle?: {}
}

export type toggleObject = {
  [index: string]: boolean
}
