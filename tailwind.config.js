module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontSize: {
      // relative
      'xxs': '.5em',
      'xs': '.75em',
      'sm': '.875em',
      'tiny': '.875em',
      'base': '1em',
      'lg': '1.125em',
      'xl': '1.25em',
      '2xl': '1.5em',
      '3xl': '1.875em',
      '4xl': '2.25em',
      '5xl': '3em',
      '6xl': '4em',
      '7xl': '5em',
      // absolute
      'rxxs': '.5rem',
      'rxs': '.75rem',
      'rsm': '.875rem',
      'rtiny': '.875rem',
      'rbase': '1rem',
      'rlg': '1.125rem',
      'rxl': '1.25rem',
      'r2xl': '1.5rem',
      'r3xl': '1.875rem',
      'r4xl': '2.25rem',
      'r5xl': '3rem',
      'r6xl': '4rem',
      'r7xl': '5rem',
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography')
  ]
}
